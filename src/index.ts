const title = 'Hello TypeScript' as string;

document.getElementById('content')!.innerHTML = `${title}`;

console.log(title, stringYoYo(title));
import {Personage} from "../clases/Personage";
let syder = new  Personage("syder",100,30,1,0,"he");

/**
 * Convertit une lettre sur 2 en majuscule
 * @param title 
 */
function stringYoYo(title: string): string
{
    const arr = title.split('');

    const newArr = arr.map((letter, index) => {
        if (index % 2 == 0) 
        {
            return letter.toUpperCase();
        }
        
        return letter;
        
    });

    return newArr.join('');
}